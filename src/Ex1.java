
public class Ex1 {
}

class A {

}

class B extends A {
    private E e;
    private String param;
    //Association
    private Z z;

    public B(E e) {
        this.e = e;
    }

    public void setZ(Z z) {
        this.z = z;
    }
}

class E {

}

class C {
    private B b;

    public C(B b) {
        this.b = new B(new E());
    }
}

class D {
    public void f() {
        System.out.println();
    }

    ;

    // dependence
    public void dependent(B b) {
        System.out.println(b.toString());
    }
}

class Z {
    public void g() {
    }


}

