
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;



public class Ex2 {
    public static void main(String[] args) {
        new Win();
    }
}
class Win extends JFrame {

    JButton button1;
    JTextField field1;
    JTextArea textArea;
    JScrollPane scrollPane = new JScrollPane(textArea);
    int chars;

    public Win() {
        super("Colocviu");
        init();
        this.setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void init() {
        this.setLayout(null);
        this.setBounds(500, 100, 275, 350);

        button1 = new JButton("Copy-Paste");
        button1.setBounds(50, 100, 150, 25);
        this.add(button1);

        field1 = new JTextField();
        field1.setBounds(50, 50, 150, 25);
        this.add(field1);

        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = field1.getText();
                textArea.append(text);
                field1.setText("");
            }
        });

        textArea = new JTextArea(10, 20);
        textArea.setBounds(50, 150, 150, 150);
        textArea.setLineWrap(true);
        textArea.setEditable(false);
        this.add(textArea);
    }
}